# Copyright 2020 Nicholas Ayers
# Licensed under GPLv2 or later
# Refer to COPYING file included

import signal
import sys
import json
import datetime
from cleanDict import makeCleanList
from getkey import getkey, keys
from blessings import Terminal
from getCompleted import *
from getdict import *

from helper import *
from TopBar import *
from SideBar import *
from ListSelector import *
from MediaEntryBar import *
from editorBar import *

from main_class import *

#globals
t = Terminal()

#startup

#post start of log

time = datetime.datetime.now()

with open(".log.txt", "a") as log:
    
    print("\n" + str(sys.argv)+" : ", file = log, end ="")
    print(time.strftime("%Y-%m-%d %H:%M:%S"), file = log)
    
if("-cl" in sys.argv):
    with open(".log.txt",'w') as log:
        print("Log Cleared"+time.strftime("%Y-%m-%d %H:%M:%S"),file=log)
    print("Log Cleared")
    exit()


if("-o" not in sys.argv):
    try:
        al, ml = updateDictionary()
        print("updateDictiony")
        al = trimList(al)
        ml = trimList(ml)
        saveLists(al, ml)
        ANIME_LIST = makeCleanList(al)
        MANGA_LIST = makeCleanList(ml)
    except Exception as e:
        raise(e)
        err("fetch list error", True)
else:
    try:
        ANIME_LIST = readAnimeList()
        MANGA_LIST = readMangaList()
    except Exception as e:
        raise e
        print(t.exit_fullscreen(),end="")
        print("JsonFileError Main()")
        print(e)
        err("jsonFileError Main()", True)
        
if("-u" in sys.argv):
    exit()

print(t.enter_fullscreen(),end="")

ANIMELIST = ["Watching", "Completed", "Paused", "Dropped", "Planning"]
MANGALIST = ["Reading", "Completed", "Planning"]            

def exit(sig, frame):
    print(t.exit_fullscreen(),end="")
    print("ctrl-c pressed")
    #print(sb.list)
    sys.exit(0)

        

#main
signal.signal(signal.SIGINT, exit)


        
# tb = TopBar(0,t)
# sidebar_media = SideBar(t)
# sidebar_lists = ListSelector(t)
# sidebar_editor = EditorBar(t)
# entry_current = ANIME_LIST["status"]["Completed"][0]
# sidebar_editor.setUp("STATUS", ANIME_LIST, ANIME_LIST["status"]["Completed"][0])
# #sidebar_current = sidebar_editor
# sidebar_current = sidebar_lists
# #sidebar_current = sidebar_media
# mb = MediaEntryBar(t)
# #tb.print()

#LISTS = (ANIME_LIST, MANGA_LIST)
#list_number = 0;
if("-m" in sys.argv):
    list_number = 1
else:
    list_number = 0


# current_list = LISTS[list_number]["status"]["Completed"]
# sidebar_current.list = current_list
# sidebar_current.list = list(LISTS[list_number]["status"].keys())
# sidebar_current.list += list(LISTS[list_number]["customList"].keys())
# sidebar_current.makePages()

# tb.print(list_number)
# sidebar_current.print()
# #mb.print(sidebar_media.getCurrent())

# input_target = sidebar_current
# getInput()
# tb.print(list_number)
# sidebar_current.print()

main = Main(ANIME_LIST, MANGA_LIST, t, list_number)


#entry = ANIME_LIST["status"]["Planning"][0]

#entry["score"] = 5
#entry["status"] = "PAUSED"
#token = ''
#with open("anilist.txt",'r') as rp:
#    token = rp.read()

#postUpdate(entry, token.strip())

#exit(1,1)
with t.hidden_cursor():
    while True:
        if not main.run():
            exit(1,1)


# while True:
    
#     if(sidebar_current == sidebar_media):
#         entry_current = sidebar_current.getCurrent()
#         mb.print(entry_current)
#     elif(mb.selected):
#         mb.print(entry_current)
    
#     key = getInput()
#     #err(type(input_target))
#     #if(input_target == mb): exit()
    
#     if(key == "a" or key == "m"):
#         if(key == "a"): list_number = 0
#         else: list_number = 1
#         sb = sidebar_lists
#         #sb.list = LISTS[list_number]["data"]["MediaListCollection"]["lists"]
#         tb.print(list_number)
#         if isinstance(sb,SideBar):
#             sidebar_current = sb
#             input_target = sb
#             temp_list = list(LISTS[list_number]["status"].keys())
#             temp_list += list(LISTS[list_number]["customList"].keys())
#             sidebar_current.reset(temp_list)
#             sidebar_current.makePages()
#             sidebar_current.print(-1)
#             mb.input("unselect")
#             mb.clear()

#     #--------------MAIN CHECK---------------------------------------------------------------------
    
#     elif(key == 'b' or key == 'e'):
#         if(isinstance(input_target, EditorBar)):
#             if(key == 'e'): input_target.select()
#             else:
#                 if(input_target.exit()):
#                     sidebar_current = sidebar_media
#                     input_target = mb
#                     sidebar_current.print()
#         elif(isinstance(input_target, SideBar)):
#             sb = selectSideBar(LISTS[list_number], sidebar_current.input(key), sidebar_lists, sidebar_media)
#             if isinstance(sb,SideBar):
#                 sidebar_current = sb
#                 input_target = sb
#                 sidebar_current.print(-1, mb)
#         elif(isinstance(input_target, MediaEntryBar)):
#             if(key == 'e'):
#                 result = mb.input(key)
#                 if("CL:" in result):
#                     cl_name = result[3:]
#                     swapCustomList(LISTS[list_number], entry_current,cl_name)
#                     continue
#                 sidebar_editor.setUp(result,LISTS[list_number],entry_current)
#                 input_target = sidebar_editor
#                 sidebar_current = sidebar_editor
#                 sidebar_current.print()
#                 err(sidebar_current.list)
#     elif(key == '1' or key == '-1'):
#         input_target.input(key, mb)
#     elif(key == 't'):
#         if(isinstance(input_target, MediaEntryBar)):
#             mb.input("unselect")
#             input_target = sidebar_current
#         elif(isinstance(input_target, SideBar) and sidebar_current == sidebar_media):
#             input_target = mb
#             mb.input("select")

