# Copyright 2020 Nicholas Ayers
# Licensed under GPLv2 or later
# Refer to COPYING file included

from getkey import getkey, keys
from getCompleted import *

#functions

def err(mess, q = False):
    with open(".log.txt", "a") as err:
        print(mess, file=err)
    if(q):
        quit()
    
def getNonNull(*args):
    for item in args:
        if item != None:
            return item
    
def getTitle(item):
    if "media" in item:
        return item["media"]["title"]["userPreferred"]
    elif "name" in item:
        return item["name"]
    else:
        return item
    
def addNewLines(title, max_length):
    title = title.replace("\n", "")
    for j in range(0, len(title), max_length):
        p1 = title[:j]
        p2 = title[j:]
        p1 = p1.strip()
        p2 = p2.strip()
        title = p1 + "\n" + p2 + "\n"
    return title.strip()

def selectSideBar(FULL_LIST, return_value, sidebar_lists, sidebar_anime):
    
    STATUS = ("Watching", "Completed", "Paused", "Dropped", "Planning", "Reading")

    if return_value in STATUS:
        sub_list = "status"
    else:
        sub_list = "customList"
    
    if(type(return_value) == str):
        if(return_value == "back_main"):
            return sidebar_lists
        sidebar_anime.reset(FULL_LIST[sub_list][return_value])
        sidebar_anime.makePages()
        return sidebar_anime
    else:
        return None

def getInput():
    key = getkey()
    if(key == keys.F1): return "a"
    elif(key == keys.F2): return "m"
    elif(key == keys.ENTER): return 'e'
    elif(key == keys.BACKSPACE):return 'b'
    elif(key == keys.UP): return '-1'
    elif(key == keys.DOWN): return '1'
    elif(key == keys.TAB) :return 't'
    elif(key == 'q'): return 'q'
    elif(key == 'f'): return 'f'
