# Copyright 2020 Nicholas Ayers
# Licensed under GPLv2 or later
# Refer to COPYING file included

from helper import *
import copy

class MediaEntryBar:
    x = 33
    y = 2
    width = 0
    current = "STATUS"
    selected = False
    entry = None
    entry_cp = None
    BASE_SELECT_LIST = ["STATUS", "PROGRESS", "SCORE"]
    
    select_list = []
    
    def __init__(self, terminal):
        self.width = terminal.width - self.x
        self.terminal = terminal
        pass

    def sel(self, name, base):
        if self.selected and name == self.current:
            return self.terminal.underline_red(base)
        else:
            return base
        
    def print(self, entry=None):
        if self.selected:
            if(self.entry_cp == None and self.entry == None):
                return False
            if(entry_cp != None):
                entry = self.entry_cp
            else:
                entry = self.entry
        elif(entry == None):
            if(self.entry == None): return
            elif(self.entry_cp != None):
                entry = self.entry_cp
            else: entry = self.entry
        else:
            #err("entry:")
            self.entry = entry
            self.entry_cp = None
            self.setupCopy()
        self.buildSelectList(entry)
        #self.clear()
        t = self.terminal
        media = entry["media"]
        isAnime = (media["type"] == "ANIME")
        
        with t.location(self.x,self.y):
            #Title
            try:
                title = media["title"]["userPreferred"].replace("\n", "")
                title_2 = title[self.width:]
                title = title[:self.width]
                print(t.bold_blue_underline(title))
                print(t.move(3,33) + t.bold_blue_underline(title_2))
            except KeyError as e:
                err("TitleError MediaEntryBar.print()\n" + e)
            #Status
            try:
                progress = entry["progress"]
                total = getNonNull(media["episodes"],media["chapters"],0)
                print(t.move(4,33) + "Status: ", end="")
                print(self.sel("STATUS", entry["status"]), end="")
                print(self.sel("PROGRESS", "(%d/%d)" %(progress,total)))
            except KeyError as e:
                err("StatusError MediaEntryBar.print():\n" + e)
            #WatchInterval
            try:
                start = ""
                end = ""
                if entry["startedAt"]["year"] or self.selected:
                    start = str(entry["startedAt"]["year"]) + "-"
                    start += str(entry["startedAt"]["month"]) + "-"
                    start += str(entry["startedAt"]["day"])
                if entry["completedAt"]["year"] or self.selected:
                    end = str(entry["completedAt"]["year"]) + "-"
                    end += str(entry["completedAt"]["month"]) + "-"
                    end += str(entry["completedAt"]["day"])
                print(t.move(4,62) + self.sel("startedAt","%s" %(start)))
                print(t.move(5,62) + self.sel("completedAt","%s" %(end)))
            except KeyError as e:
                err("WatchIntervalError MediaEntryBar.print()\n" + e)

            #Rewatch/Score
            try:
                print(t.move(6,33) + self.sel("SCORE","Score: %d" %(entry["score"])))
            except KeyError as e:
                err("ScoreError MediaEntryBar.print()\n" + e)

            try:
                rewatches = entry["repeat"]
                if (rewatches >=1 or self.selected):
                    print(t.move(6,43) + self.sel("REWATCH","(Rewatches: %d)" %(rewatches)))
            except KeyError as e:
                err("Rewatch Error MediaEntryBar.print()\n" + e)

                
            #CustomLists
            try:
                if entry["customLists"] != None:
                    i = 0
                    with t.location(33,8):
                        for key in entry["customLists"]:
                            if(i%4 == 3):
                                print(t.move(8+((i+1)//4),33),end="")
                            if entry["customLists"][key]:
                                name = ("*" + key[:12] + "*").ljust(12)
                                name = t.bold_green(self.sel("CL:"+key,name))
                                print(name,end="")
                            else:
                                name = key[:12].ljust(12)
                                name = self.sel("CL:"+key,name)
                                print(name,end="")
                            i += 1
            except KeyError as e:
                err("CustomListError MediaEntryBar.print()\n" + e)
            print(t.move(t.height-12, 33) + ('-'*(t.width-33)))


            print(t.move(t.height-11,33),end="")
            #Studio
            try:
                if media["type"] == "ANIME":
                    studio = media["studios"]["edges"][0]["node"]["name"]
                    for edge in media["studios"]["edges"]:
                        if edge["isMain"]:
                            studio = edge["node"]["name"]
                    
                    print(t.bold_underline(studio),end="")
                    print(t.move_x(57) + media["source"])
            except KeyError as e:
                err("StudioError MediaEntryBar.print()\n" + e)

            #AirInterval
            try:
                if( media["startDate"]["year"]):
                    if(media["season"]):
                        print(t.move(t.height-10,33)+t.bold_cyan(media["season"]),end="")
                    print(t.bold_cyan(" " + str(media["startDate"]["year"])+"-"),end="")
                    print(t.bold_cyan(str(media["startDate"]["month"])+"-"),end="")
                    print(t.bold_cyan(str(media["startDate"]["day"])+" - "), end="")
                if(media["endDate"]["year"]):
                    print(t.bold_cyan(str(media["endDate"]["year"])+"-"),end="")
                    print(t.bold_cyan(str(media["endDate"]["month"]) + "-"),end="")
                    print(t.bold_cyan(str(media["endDate"]["day"])))
            except KeyError as e:
                err("AirIntervalError MediaEntryBar.print()n\" + e")

            
            print(t.move(t.height-9, 33),end="")

            #Genre
            try:
                genres = []
                for genre in media["genres"]:
                    genres.append(genre)
                gcount = 0
                for genre in genres:
                    if(gcount%5 == 4):
                        print(t.move(t.height - 9 + gcount//5,33),end="")
                    print(genre,end=" ")
                    gcount += 1
                with t.location(33, t.height - 1):
                    if(media["title"]["english"]):
                        print(media["title"]["english"][:t.width - self.x].strip(),end = "")
                print(t.move(0,0) + "")
            except KeyError as e:
                err("GenreError MediaEntryBar.print()\n" + e)

    def buildSelectList(self,entry):
        self.select_list = self.BASE_SELECT_LIST.copy()
        try:
            if(entry["repeat"] > 0 or self.selected):
                self.select_list.append("REWATCH")
        except KeyError as e:
            err("RewatchError: buildSelectList\n" + e)
        try:
            self.select_list.append("startedAt")
            self.select_list.append("completedAt")
            if(entry["customLists"]):
                for key in entry["customLists"]:
                    self.select_list.append("CL:" + key)
        except KeyError as e:
            err("CLErrror: buildSelectList\n" + e)


    def cancel(self):
        if(self.entry == None or self.entry_cp == None): return
        err("CANCLEING UPDATE")
        err("cp_status:%s, en_status:%s" %(self.entry_cp["status"], self.entry["status"]) )
        self.entry.clear()
        for i, j  in self.entry_cp.items():
            self.entry[i] = j
            err("%s : %s" %(i,j))
            
    def setupCopy(self):
        if(self.entry == None): return
        self.entry_cp = copy.deepcopy(self.entry)
        #self.entry_cp["EDITS"] = {"CL":[], "STATUS":"", "SCORE":0, "startedAt":[0,0,0], "completedAt":[0,0,0], "REWATCH": 0}
        
            
        
    def input(self, i, mb=-1):
        if(i == 'e'):
            #if("CL:" in self.current): return self.current[3:]
            #else: return self.current
            
            return self.current
        elif(i == "select"):
            self.selected = True
        elif(i == "unselect"):
            self.current = "STATUS"
            self.selected = False
        elif(i == '1' or i == '-1'):
            if(self.current in self.select_list):
                index = self.select_list.index(self.current)
                if(index==0 and i=='-1'): return -1
                if(index==len(self.select_list)-1 and i=='1'): return -1
                self.current = self.select_list[index + int(i)]
                return
    def clear(self):
        t = self.terminal
        with t.location(30,5):
            for i in range(self.y, t.height):
                print(t.move(i,self.x) + (" "*self.width),end='')
        print(t.move(0,0))

    def message(self, num = 0):
        if(num == 0):
            with self.terminal.location(33,2):
                print(self.terminal.bold_red_underline("This Software is lisenced under the GPLv2 or later"))
