# Copyright 2020 Nicholas Ayers
# Licensed under GPLv2 or later
# Refer to COPYING file included

from SideBar import SideBar
from editorFuncs import *
from helper import *
import datetime
from DateBar import *
class EditorBar(SideBar):
    def __init__(self, terminal):
        self.list = []
        self.page = 0
        self.active = 0
        self.t = terminal
        self.FULL_LIST = None
        self.entry = None
        self.entry_cp = None
        self.mode = ""
        self.submode = ""
    def setUp(self, mode, FULL_LIST, entry, entry_cp, submode=""):
        self.clear()
        self.list = []
        self.submode = submode
        self.FULL_LIST = FULL_LIST
        self.entry = entry
        self.entry_cp = entry_cp
        self.mode = mode
        self.page = self.active = 0

        set_active = 0
        #DATEEDITOR
        if(mode in ("startedAt", "completedAt")):
            year = self.entry[self.mode]["year"]
            month = self.entry[self.mode]["month"]
            day = self.entry[self.mode]["day"]
            self.sub_bar = DateBar(self.t,year,month,day)
            self.sub_bar.setUp(0)
            self.list = ['dummy']
                
        elif(mode == "STATUS:A" or mode == "STATUS"):
            self.list = ["Completed", "Paused", "Planning", "Dropped", "Watching"]
        elif(mode == "SCORE"):
            self.list = ['0','1','2','3','4','5','6','7','8','9','10']
        elif(mode == "PROGRESS"):
            top = 999
            base_str = entry["media"]["type"]
            if(base_str == "ANIME"):
                if(entry["media"]["episodes"]):
                    top = entry["media"]["episodes"]
            else:
                if(entry["media"]["chapters"]):
                    top = entry["media"]["chapters"]
                    
            int_list = list(range(1,top+1))
            self.list = [str(i) for i in int_list]
        elif(mode == "REWATCH"):
            int_list = list(range(0,1000))
            self.list = [str(i) for i in int_list]
        self.makePages()
        #if(set_active == -1):
        #    self.page = len(self.list) - 1
        #    self.active = len(self.list[self.page]) - 1
    
    def format(self):
        for i in range(len(self.list)):
            self.list[i] =self.list[i].strip() + "\n"
        
    def select(self):
        current = self.list[self.page][self.active].strip()
        #err("test")

        if(self.mode in ("startedAt", "completedAt")):
            self.sub_bar.select()
            self.setDate(self.sub_bar.getCurrent())
            
        elif(self.mode == "STATUS"):
            self.entry_cp["status"] = current
            #changeStatus(self.FULL_LIST, self.entry_cp, current)
        elif(self.mode == "SCORE"):
            self.entry_cp["score"] = int(self.list[self.page][self.active])
        elif(self.mode == "PROGRESS"):
            self.entry_cp["progress"] = int(self.list[self.page][self.active])
        elif(self.mode == "REWATCH"):
            count = int(self.list[self.page][self.active])
            self.entry_cp["repeat"] = count

    def setDate(self, date):
        self.entry_cp[self.mode]["year"] = date[0]
        self.entry_cp[self.mode]["month"] = date[1]
        self.entry_cp[self.mode]["day"] = date[2]
            
