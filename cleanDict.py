# Copyright 2020 Nicholas Ayers
# Licensed under GPLv2 or later
# Refer to COPYING file included

from getdict import *

def trimList(base_list):
    t_list = base_list["data"]["MediaListCollection"]["lists"]
    trimmed_list = {}
    for l in t_list:
        if(l["isCustomList"]):
            t_list.remove(l)
    for i in t_list:
        trimmed_list[i["name"]] = i["entries"]
    return trimmed_list

def makeCleanList(trimmed_list):
    clists = {}
    statuses = {}
    #get cl names
    if(len(trimmed_list["Completed"]) > 0 and trimmed_list["Completed"][0]["customLists"]):
        for cl in trimmed_list["Completed"][0]["customLists"]:
            clists[cl] = []
    for s in trimmed_list:
        statuses[s] = trimmed_list[s]
    clean_list = {"status" : statuses, "customList" : clists}
    for status in clean_list["status"]:
        for entry in clean_list["status"][status]:
            if(entry["status"]):
                entry["status"] = entry["status"].title()
            if(entry["customLists"]):
                for cl in entry["customLists"]:
                    if entry["customLists"][cl]:
                        clean_list["customList"][cl].append(entry)

    return clean_list


#al, ml = updateDictionary()
#ca = makeCleanList(al)
#cm = makeCleanList(ml)
#saveLists(ca, cm)

#for a in clean["status"]["Completed"]:
#    a["score"] = 1

#for a in clean["customList"]["SkipMusic"]:
#    print("%s: %d" %(a["media"]["title"]["userPreferred"], a["score"]))
    
