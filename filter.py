#Status
#Start/End Watch date
#Genres
#Score
#Custom Lists

#Start/End air date

from helper import *

class FilterRange:
    default = False
    def check(self, other):
        is_in = False
        try:
            if(other == None or None in other): return self.default
        except TypeError:
            pass
        if(self.start < other and other < self.end):
            is_in = True
        if(self.include): return is_in
        return not is_in

class ScoreRange(FilterRange):
    def __init__(self, s1, s2, include = True):
        self.start = (s1-1)
        self.end = (s2+1)
        self.include = include
        
class DateRange(FilterRange):
    def __init__(self, y1, m1, d1, y2, m2, d2, include = True):
        self.start = (y1,m1,d1)
        self.end = (y2,m2,d2)
        self.include = include
    

class Filter:
    def __init__(self, status, start_air = None, end_air = None, start_watch = None, end_watch = None, score_range = None, custom_lists = {}, prefab = 0):
        if(prefab != 0): status == "Completed"
        if(prefab == 1):
            start_air = DateRange(2000,1,1, 2010,1,1)
        elif prefab == 2:
            end_air = DateRange(2000, 1, 1, 2010, 1, 1)
        elif prefab == 3:
            start_watch = DateRange(2020, 6,1, 2020, 12, 31)
        elif prefab == 4:
            end_watch = DateRange(2020, 6, 1, 2020, 12,31)
        elif prefab == 5:
            score_range = ScoreRange(4,6)
        elif prefab == 6:
            score_range = ScoreRange(8,10, False)
        elif prefab == 7:
            custom_lists = {"HasMusic": True, "Defy": True}
        
        self.status = status
        self.start_air = start_air
        self.end_air = end_air
        self.start_watch = start_watch
        self.end_watch = end_watch
        self.score_range = score_range
        self.custom_lists = custom_lists

    def build(self, full_list):
        if(type(full_list) != dict): return []
        if(self.status not in full_list["status"]): return []
        rax = []
        for entry in full_list["status"][self.status]:
            if(self.check(entry)):
                rax.append(entry)
        return rax
    
    def check(self, entry):
        if("media" not in entry): raise Exception("Invalid entry in Filter")
        if(not self.checkStatus(entry)): return False
        if(not self.checkAir(entry["media"])): return False
        if(not self.checkWatch(entry)): return False
        if(not self.checkScore(entry)): return False
        if(not self.checkCL(entry)): return False
        return True


    def checkAir(self,media):
        try:
            start = (media["startDate"]["year"],media["startDate"]["month"],media["startDate"]["day"])        
            if(isinstance(self.start_air, FilterRange)):
                if(not self.start_air.check(start)): return False
        except KeyError as E:
            err("checkAir")
            if(self.start_air and not self.start_air.default): return False;
        try:
            end = (media["endDate"]["year"],media["endDate"]["month"],media["endDate"]["day"])
            if(isinstance(self.end_air, FilterRange)):
                if(not self.end_air.check(end)): return False
        except KeyError as E:
            err("checkAir2")
            if(self.end_air and not self.end_air.default): return False    
        return True

    def checkWatch(self, entry):
        try:
            start = (entry["startedAt"]["year"],entry["startedAt"]["month"],entry["startedAt"]["day"])        
            if(isinstance(self.start_watch, FilterRange)):
                if(not self.start_watch.check(start)): return False
        except KeyError as E:
            err("checkWatch")
            if(self.start_watch and not self.start_watch.default): return False;
        try:
            end = (entry["completedAt"]["year"],entry["completedAt"]["month"],entry["completedAt"]["day"])
            if(isinstance(self.end_watch, FilterRange)):
                if(not self.end_watch.check(end)): return False
        except KeyError as E:
            err("checkWatch2")
            if(self.end_watch and not self.end_watch.default): return False
        return True

    def checkScore(self, entry):
        try:
            score = entry["score"]
            if(isinstance(self.score_range, FilterRange)):
                return self.score_range.check(score)
        except KeyError as E:
            err("checkScore")
        if(isinstance(self.score_range, FilterRange)):
            return self.score_range.default
        else:
            return True

    def checkStatus(self, entry):
        if(self.status.upper() not in ("CURRENT", "COMPLETED", "WATCHING", "PAUSED", "DROPPED", "PLANNING", "READING")): return True
        try:
            s = entry["status"]
            if(type(s) != str or type(self.status) != str): return True
            if(s.upper() == self.status.upper()):
                return True
            else:
                err("%s : %s" %(s.upper(), self.status.upper()))
                return False
        except KeyError as E:
            err(str(E))
            return True

    def checkCL(self, entry):
        try:
            entry_cl = entry["customLists"]
        except KeyError as E:
            return True

        if("mode" not in self.custom_lists or custom_lists["mode"] == "DEFAULT"):
            for l in self.custom_lists:
                if(l == "mode"): continue
                if(self.custom_lists[l]):
                    if(l not in entry_cl or entry_cl[l] == False): return False
                    else: pass
                else:
                    if(l in entry_cl and entry_cl[l] == True): return False
            return True
        return False
    
