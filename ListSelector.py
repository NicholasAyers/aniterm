# Copyright 2020 Nicholas Ayers
# Licensed under GPLv2 or later
# Refer to COPYING file included

from SideBar import SideBar
from helper import *

class ListSelector(SideBar):
    def __init__(self, terminal):
        self.list = []
        self.page = 0
        self.active = 0
        self.t = terminal
        
    def format(self):
        for i in range(len(self.list)):
            self.list[i] =self.list[i].strip() + "\n"

    def select(self):
        return self.list[self.page][self.active].strip()
