import signal
import sys
import json
from getkey import getkey, keys
from blessings import Terminal

from helper import *
from TopBar import *
from SideBar import *
from ListSelector import *
from MediaEntryBar import *
from editorBar import *
from editorFuncs import *
from DateBar import *

class Main:
    t = None
    tb = None
    mb = None
    sidebar_media, sidebar_lists, sidebar_editor = [None]*3
    entry_current = None
    online = False
    LISTS = []
    list_number = 0
    
    def run(self):
        if(self.mb.selected):
            pass
            #self.mb.print()
        elif(self.sidebar_current == self.sidebar_media):
           self.entry_current = self.sidebar_current.getCurrent()
           #self.mb.print(self.entry_current)
        #elif(self.sidebar_current == self.sidebar_lists):
        #    self.mb.clear()
        key = getInput()
        #Switch Mode
        if(key == "a" or key == "m"):
            self.switchMode(key)
        #pass movemnt 
        elif(key == '1' or key == '-1'):
            key_r = self.input_target.input(key, self.mb)
            if(key_r != -1):
                self.entry_current = self.sidebar_media.getCurrent()
                if(self.input_target == self.sidebar_media):
                    self.mb.clear()
                if(self.sidebar_current != self.sidebar_lists):
                    self.mb.print(self.entry_current)
        elif(key == 't'):
           self.swapBar()
        elif(key == 'e'):
            self.enter(key)
        elif(key == 'b'):
            self.cancel(key)
        elif(key == 'f' and self.input_target == self.sidebar_media):
            filt = Filter("Completed", prefab = 7)           
            new_list = filt.build(self.LISTS[0])
            if(len(new_list) == 0): return True
            self.input_target.reset(new_list)
            self.input_target.makePages()
            self.input_target.print()
            self.updateCurrent()
        elif(key == 'q' and self.sidebar_current == self.sidebar_lists):
            if(self.online):
                saveLists(self.LISTS[0]["status"], self.LISTS[0]["status"])
            return False
        return True


    def enter(self, key):
        if(isinstance(self.input_target, EditorBar)):
            self.input_target.select()
            self.mb.clear()
            self.mb.print()
        elif(isinstance(self.input_target, MediaEntryBar)):
            result = self.mb.input(key)
            if("CL:" in result):
                cl_name = result[3:]
                swapCustomList(self.LISTS[self.list_number], self.mb.entry_cp ,cl_name)
                return
            self.sidebar_editor.setUp(result,self.LISTS[self.list_number],self.mb.entry, self.mb.entry_cp)
            self.input_target = self.sidebar_editor
            self.sidebar_current = self.sidebar_editor
            self.sidebar_current.print(-1)
            #err(sidebar_current.list)
        elif(isinstance(self.input_target, SideBar)):
            self.enterSB(key)

    def enterSB(self, key):
        sb = selectSideBar(self.LISTS[self.list_number], self.sidebar_current.input(key), self.sidebar_lists, self.sidebar_media)
        if isinstance(sb,SideBar):
                self.sidebar_current = sb
                self.input_target = sb
                self.sidebar_current.clear()
                self.mb.clear()
                self.updateCurrent()

    def updateCurrent(self):
        if(self.sidebar_current == self.sidebar_media):
            self.entry_current = self.sidebar_media.getCurrent()
            self.mb.print(self.entry_current)
            self.sidebar_current.print(-1, self.mb)

                
    def cancel(self, key):
        if(isinstance(self.input_target, SideBar)):
            if(not self.input_target.exit()):
                return 
        if(isinstance(self.input_target, EditorBar)):
            if(self.input_target.exit()):
                self.sidebar_current = self.sidebar_media
                self.input_target = self.mb
                self.sidebar_current.print()
        elif(isinstance(self.input_target, SideBar)):
            self.enterSB(key)
        elif(isinstance(self.input_target, MediaEntryBar)):
            #self.mb.cancel()
            self.mb.input("unselect")
            self.input_target = self.sidebar_current
            self.mb.clear()
            self.mb.print()
            
    def swapBar(self):
        if(isinstance(self.input_target, MediaEntryBar)):
            result = update(self.LISTS[self.list_number],self.mb.entry, self.mb.entry_cp)
            if(result and self.token != ''):
                postUpdate(self.mb.entry, self.token)
            self.mb.input("unselect")
            self.mb.clear()
            self.mb.print()
            self.input_target = self.sidebar_current
        elif(type(self.input_target) == SideBar and self.login):
            self.input_target = self.mb
            self.mb.input("select")
            self.mb.print()
            
    def switchMode(self,key):
        if(key == 'a' and self.list_number == 0): return
        elif(key == 'm' and self.list_number == 1): return
        if(key == "a"): self.list_number = 0
        else: self.list_number = 1
        self.sidebar_current = self.sidebar_lists
        self.input_target = self.sidebar_current
        
        self.tb.print(self.list_number)
        temp_list = list(self.LISTS[self.list_number]["status"].keys())
        temp_list += list(self.LISTS[self.list_number]["customList"].keys())
        self.sidebar_current.reset(temp_list)
        self.sidebar_current.makePages()
        self.sidebar_current.print(-1)
        self.mb.input("unselect")
        self.mb.clear()
           
    
    def __init__(self,anime_list, manga_list, t, MANGA = False):
        self.t = t
        self.tb = TopBar(0,t)
        self.sidebar_media = SideBar(t)
        self.sidebar_lists = ListSelector(t)
        self.sidebar_editor = EditorBar(t)
        self.mb = MediaEntryBar(t)
        self.sidebar_current = self.sidebar_lists
        self.entry_current = anime_list["status"]["Completed"][0]
        self.online = (not ('-o' in sys.argv))
        self.login = ('-l' in sys.argv)
        self.LISTS = [anime_list, manga_list]
        if MANGA:
            self.list_number = 1;
        else:
            self.list_number = 0;

        self.current_list = self.LISTS[self.list_number]["status"]["Completed"]
        self.sidebar_current.list = self.current_list
        self.sidebar_current.list = list(self.LISTS[self.list_number]["status"].keys())
        self.sidebar_current.list += list(self.LISTS[self.list_number]["customList"].keys())
        self.sidebar_current.makePages()

        self.tb.print(self.list_number)
        self.sidebar_current.print()
        
        self.input_target = self.sidebar_current
        self.mb.message()
        #getInput()
        self.tb.print(self.list_number)
        #self.sidebar_current.print()
        self.token = ''
        #self.login = self.online
        try:
            with open("anilist.txt",'r') as rp:
                self.token = rp.read().strip()
                if(not ('-o' in sys.argv and not'-l' in sys.argv) and self.token):
                    self.login = True
        except:
            pass
            self.login = False
        if not self.online:
            self.tb.offline()
        if self.login:
            self.tb.login()
        self.tb.print(self.list_number)
        #self.sidebar_list = DateBar(t)
        #self.sidebar_current = self.sidebar_list
        #self.input_target = self.sidebar_current
        #self.sidebar_current.setUp(0)
        #self.sidebar_current.clear()
        #self.sidebar_current.print()
