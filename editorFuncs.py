# Copyright 2020 Nicholas Ayers
# Licensed under GPLv2 or later
# Refer to COPYING file included

from helper import *
import json
import requests

def update(full_list, entry, entry_cp):
    result = False
    
    if(entry_cp["status"] != entry["status"]):
        result = True
        changeStatus(full_list, entry, entry_cp["status"])

    if(entry["startedAt"] != entry_cp["startedAt"]):
        result = True
        entry["startedAt"] = entry_cp["startedAt"]
        
    if(entry["completedAt"] != entry_cp["completedAt"]):
        result = True
        entry["completedAt"] = entry_cp["completedAt"]

    if(entry["score"] != entry_cp["score"]):
        result = True
        entry["score"] = entry_cp["score"]

    if(entry["progress"] != entry_cp["progress"]):
        result = True
        entry["progress"] = entry_cp["progress"]

    if(entry["repeat"] != entry_cp["repeat"]):
        result = True
        entry["repeat"] = entry_cp["repeat"]
    

    for i in entry["customLists"]:
        if(entry["customLists"][i] != entry_cp["customLists"][i]):
            result = True
            swapCustomList(full_list, entry, i, True)
    return result


def changeStatus(full_list, entry, status):
    if("status" not in full_list):
        err("invalid list: changeStatus()", True)
    if(status not in full_list["status"]):
        err("invalid destination status: changeStatus()", True)
    if("status" not in entry):
        err("missing status key in entry: changeStatus()", True)
    if(entry["status"] not in full_list["status"]):
        print("Status: %s" %(entry["status"]))
        err("missing source status: changeStatus()", True)
    if(entry not in full_list["status"][entry["status"]]):
        err("entry not in source status: changeStatus()", True)

    try:
        full_list["status"][entry["status"]].remove(entry)
        full_list["status"][status].append(entry)
        entry["status"] = status
    except KeyError:
        err("misc error: changeStatus()", True)


def swapCustomList(full_list, entry, cl_name, edit_lists = False):
    try:
        if(entry["customLists"]):
            current_val = entry["customLists"][cl_name]
        else: return
        entry["customLists"][cl_name] = not entry["customLists"][cl_name]
        if(not edit_lists): return
        if(current_val):
            full_list["customList"][cl_name].remove(entry)
        else:
            full_list["customList"][cl_name].append(entry)

    except KeyError as e:
        err("KeyErrror in swapCustomList()", True)

def postUpdate(entry, token):
    try:
        custom_lists = []
        for cl in entry["customLists"]:
            if(entry["customLists"][cl]): custom_lists.append(cl)


        query = '''
    mutation ($mediaId: Int, $status: MediaListStatus, $score: Float, $progress: Int, $repeat: Int, $startedAt: FuzzyDateInput, $completedAt: FuzzyDateInput, $customLists: [String] ) {
        SaveMediaListEntry (mediaId: $mediaId, status: $status, score: $score, progress: $progress, repeat: $repeat, startedAt: $startedAt, completedAt: $completedAt, customLists: $customLists) {
            id
            status
            score
            progress
            repeat
            startedAt{
                year
                month
                day
            }
            completedAt{
                year
                month
                day
            }
            customLists
       }
    }
'''
        vari = {"mediaId":entry["mediaId"], "status":entry["status"].upper(), "score": entry["score"], "progress": entry["progress"], "repeat":entry["repeat"], "startedAt": entry["startedAt"], "completedAt": entry["completedAt"], "customLists": custom_lists}
        d = {"query": query, "variables": vari}

    
        ani_url = "https://graphql.anilist.co"
        ani_headers = {
            'Authorization': 'Bearer ' + token,
            'Content-Type': 'application/json',
            'Accept': 'application/json',
        }
        res = requests.post(url=ani_url, data=json.dumps(d), headers=ani_headers)
        if("errors" in res.text):
            raise Exception(res.text)
    except KeyError as e:
        raise Exception("KeyError in Posting update")
