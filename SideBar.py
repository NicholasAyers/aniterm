# Copyright 2020 Nicholas Ayers
# Licensed under GPLv2 or later
# Refer to COPYING file included

from helper import *
class SideBar:
    width = 32
    list = ["hello", "world"]
    sub_bar = None
    
    def __init__(self, terminal):
        self.page = 0
        self.active = 0
        self.t = terminal

    def reset(self, list=[]):
        self.page = 0
        self.active = 0
        self.list = list
        self.clear()
    def clear(self):
        for i in range(self.t.height):
            for j in range(0,self.width):
                with self.t.location(j,i):
                    print(" ", end='')
        with self.t.location(0,0):
            print("")
        
    def print(self, index=-1, mb=-1):
        if(isinstance(self.sub_bar, SideBar)):
            self.sub_bar.print()
            return
        if index == -1:
            index = self.page
        #self.clear()
        page = self.list[index]
        with self.t.location(0,0):
            for i in range(len(page)):
                if(i == self.active):
                    print(self.t.bold_underline_blue(getTitle(page[i])), end="")
                else:
                    print(getTitle(page[i]),end="")
        with self.t.location(0,0):
            print('')
        #if(mb != -1 and type(self) == SideBar):
         #   mb.print(self.getCurrent())
            
    def format(self):
        for i in range(len(self.list)):
            if type(getTitle(self.list[i])) != str:
                print(type(getTitle(self.list[i])))
                raise Exception("invalid list item in SideBar: ")
            self.list[i]["media"]["title"]["userPreferred"] = self.list[i]["media"]["title"]["userPreferred"].strip() + "\n"
            if len(getTitle(self.list[i])) <= self.width:
                continue
            #for j in range(self.width - 1, len(getTitle(self.list[i])), self.width):
                #p1 = getTitle(self.list[i])[:j]
                #p2 = getTitle(self.list[i])[j:]
                #p2 = p2.strip()
                #self.list[i]["media"]["title"]["userPreferred"] = p1 + "\n" + p2 + '\n'
            self.list[i]["media"]["title"]["userPreferred"] = addNewLines(getTitle(self.list[i]), self.width).strip() + "\n"
            

    def makePages(self):
        self.format()
        paged_list = []
        current_page = []
        current_lines = 0
        max_size = self.t.height - 1
        for item in self.list:
            nl = getTitle(item).count('\n')
            if(nl + current_lines <= max_size):
                current_lines += nl
                current_page.append(item)
            else:
                paged_list.append(current_page)
                current_page = []
                current_lines = 0
                current_lines += nl
                current_page.append(item)
        paged_list.append(current_page)
        self.list = paged_list
            
    def input(self,i, mb = -1):
        if(isinstance(self.sub_bar, SideBar)):
            self.sub_bar.input(i, mb)
        if(i != '1' and i != '-1' and i != 'e' and i != 'b'): raise Exception("invalid input to sidebar to move")
        #self.clear()
        if (i=='e'): return self.select()
        if (i=='b' and type(self) == SideBar):
            return "back_main"
        if(i == '1'): #move down
            if(self.active >= len(self.list[self.page]) - 1): #on final element
                if(self.page >= len(self.list) - 1): #if also on final page
                    #self.print() #do nothing
                    return -1 
                else: #move to top of next page
                    self.page += 1
                    self.active = 0
                    self.clear()
            else: #move to next element
                self.active += 1
        elif (i == '-1'): #move up
            if(self.active == 0): #on first element
                if(self.page == 0): #also on first page
                    #self.print() #do nothing
                    return -1 
                else:
                    self.page -= 1
                    self.active = len(self.list[self.page]) - 1
                    self.clear()
            else: # in the middle just move up
                self.active -= 1
        self.print(self.page, mb)

    def select(self):
        return
        
    def getCurrent(self):
        return self.list[self.page][self.active]

    def exit(self):
        if(isinstance(self.sub_bar, SideBar)):
            return self.sub_bar.exit()
        return True
