# Copyright 2020 Nicholas Ayers
# Licensed under GPLv2 or later
# Refer to COPYING file included

import json
from cleanDict import *

def getList(anime_list, name):
    completed = []
    for list in anime_list["data"]["MediaListCollection"]["lists"]:
        if(list["name"] != name):
            continue
        for entry in list["entries"]:
           completed.append(entry)
    return completed

def readMangaList():
    full_list = {}
    with open('lists_manga.json', 'r') as re:
        full_list = json.load(re)
    return makeCleanList(full_list)

def readAnimeList():
    full_list = {}
    with open('lists.json', 'r') as re:
        full_list = json.load(re)
    return makeCleanList(full_list)

#with open('completed.json', 'r') as fp:
#    completed = json.load(fp)
#    print(type(completed))
