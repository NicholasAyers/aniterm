from SideBar import SideBar
from helper import *
import datetime

class DateBar(SideBar):

    def __init__(self, terminal, y=None, m=None, d=None):
        self.list = []
        self.page = 0
        self.active = 0
        self.mode = 0
        self.year = y
        self.month = m
        self.day = d
        self.t = terminal
        
    def setUp(self, mode):
        if(mode == 1):
            now = datetime.datetime.now()
            year = now.year
            self.list = [str(y) for y in list(range(year, 1969, -1))]
        elif(mode == 2):
            self.list = ["January", "Febrary", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]
        elif(mode == 3):
            if(self.year == None or self.month == None):
                return self.setUp(0)
            if(self.month == 12):
                count = 31
            else:
                count = (datetime.date(self.year, self.month+1, 1) - datetime.date(self.year, self.month, 1)).days
            self.list = list(range(1,count+1))
            self.list = [str(d) for d in self.list]
        elif mode == 0:
            self.list = ["Year", "MONTH", "DAY", "CURRENT", "CLEAR"]
            self.list.append( (str(self.year) + "-" + str(self.month) + "-" + str(self.day)))    
        elif mode == 4:
            date = datetime.datetime.now()
            self.year = date.year
            self.month = date.month
            self.day = date.day
            self.print()
            return
        elif mode == 5:
            self.year = self.month = self.day = None
            self.print()
            return
        else:
            return
        self.mode = mode
        self.makePages()
        self.active = 0
        self.page = 0
        self.clear()
        self.print()
        
    def format(self):
        for i in range(len(self.list)):
            self.list[i] =self.list[i].strip() + "\n"

    
    def select(self):
        if(self.mode == 0):
            self.setUp(self.active + 1)
        elif(self.mode == 1):
            self.year = int(self.list[self.page][self.active])
            self.setUp(0)
        elif(self.mode == 2):
            self.month = ("January", "Febrary", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December").index(self.list[self.page][self.active].strip())+1
            self.setUp(0)
        elif(self.mode == 3):
            self.day = int(self.list[self.page][self.active])
            self.setUp(0)

    def getCurrent(self):
        return (self.year, self.month, self.day)
            
    def exit(self):
        if(self.mode == 0):
            return True
        else:
            self.setUp(0)
            return False
