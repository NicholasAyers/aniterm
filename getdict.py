# Copyright 2020 Nicholas Ayers
# Licensed under GPLv2 or later
# Refer to COPYING file included

import requests
import json
import os

def updateDictionary():
    query = '''
query ($type : MediaType, $user : String) {
    MediaListCollection(userName: $user, type: $type) {
    lists {
      name
      isCustomList
      entries {
        mediaId
        status
        score
        progress
        repeat
        startedAt {
          year
          month
          day
        }
        completedAt {
          year
          month
          day
        }
        
        customLists
        
        media {
            type
            season
            seasonYear
            episodes
            chapters
            source
            studios{
                edges {
                    isMain
                    node{
                        name
                    }
                }
            }
            genres  
            startDate {
                year
                month
                day
            }
            endDate {
            year
            month
            day
          }
            title {
            userPreferred
            english
          }					
        }
      }
    }
  }
}
'''
    variables = {
        'type': "ANIME",
        'user': "Thatsinistersylveon"
    }
    url = 'https://graphql.anilist.co'
    
    r = requests.post(url, json={'query': query, 'variables': variables})
    anime_dict = r.json();
    
    if("errors" in anime_dict):
        raise Exception(anime_dict["errors"])

    variables = {
        'type': "MANGA",
        'user': "Thatsinistersylveon"
    }
    r = requests.post(url, json={'query': query, 'variables': variables})
    manga_dict = r.json();
    
    if("errors" in manga_dict):
        raise Exception(manga_dict["errors"])

    return anime_dict, manga_dict


def saveLists(al, ml):
    with open('lists.json', 'w') as fp:
        json.dump(al, fp)

    with open('lists_manga.json', 'w') as fp:
        json.dump(ml, fp)

