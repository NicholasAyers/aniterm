# Copyright 2020 Nicholas Ayers
# Licensed under GPLv2 or later
# Refer to COPYING file included

from helper import *

class TopBar:
    catogories = ("ANIME_LIST", "MANGA_LIST", "SEARCH")
    def __init__(self, active, terminal):
        if active >=0 and active <= 2: self.active = active
        self.t = terminal 
        self.args = []
    #def input(key):
    #    self.start = False

    def print(self, list_number):
        for i in range(0, self.t.height):
            with self.t.location(32, i):
                print("|", end ='')
        with self.t.location(34,0):
            print(" "*60)
        with self.t.location(max(self.t.width - 42 - (len(self.args) +3), 0),0):
            if(len(self.args) != 0): print(self.t.red_bold  + '[', end='')
            for a in self.args:
                print(self.t.red_bold + a, end ='')
            if(len(self.args) != 0): print( self.t.red_bold + ']', end='')
        with self.t.location(max(self.t.width - 42,0), 0):
            for i in range(3):
                if(i == list_number):
                    print(self.t.bold_underline_green(self.catogories[i]),end='')
                else:
                    print(self.t.normal + self.catogories[i], end='')
                if(i < 2): print("        ", end='')
            print(" ")
            with self.t.location(33,1):
                for i in range(33, self.t.width):
                    print("-", end='')
            print("")

    def offline(self):
        if('O' not in self.args):
            self.args.append('O')

    def login(self):
        if('L' not in self.args):
            self.args.append('L')
